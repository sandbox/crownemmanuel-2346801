INTRODUCTION
------------
Provides an admin interface to customize the default time zone dropdown list.
Features
Customize the registration form dropdown list
Customize the date dropdown list
Select which timezone should show on the list
Set a custom label for each times zones

REQUIREMENTS
------------
This module requires the following modules:
 * Date (https://drupal.org/project/date)
 
 INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:
 * Configure module settings at admin/config/regional/custom_timezone
