<?php

/**
 * @file
 * Admin functions for the UI.
 */

/**
 * Admin function to manage custom timezone list.
 */
function custom_timezone_admin() {

  $timezonelists = variable_get('custom_timezone_timezonelist');
  $form = array();
  $header = array(
    'timezone' => t('Timezone'),
    'timezone_label' => t('Timezone Label'),
  );

  // Set Options for Select Table.
  $options = array();
  $defaultvalue = array();

  foreach ($timezonelists as $key => $data):
    $options[$key] = array(
      'timezone' => $key,
      'timezone_label' => array(
        'data' => array(
          '#type' => 'textfield',
          '#value' => $data['label'],
          '#name' => "custom_timezone_label[$key]",
        ),
      ),
    );
    $defaultvalue[$key] = $data['status'];
  endforeach;

  // Set customisable form options.
  $customised_forms = array(
    'alter_userform' => t('Customize user registration form timezones'),
    'alter_datefield' => t('Customize date field timezones'),
  );

  $customised_forms_options = array(
    'alter_userform' => variable_get('custom_timezone_alter_userform'),
    'alter_datefield' => variable_get('custom_timezone_alter_datefield'),
  );

  $form['custom_timezone_customised_forms'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Forms to customise'),
    '#options' => $customised_forms,
    '#default_value' => $customised_forms_options,
    '#description' => t('Select which drupal forms should be customised'),
  );

  $form['custom_timezone_timezonelists'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#default_value' => $defaultvalue,
    '#empty' => t('Unable to load the timezone list'),
  );
  $form['#submit'][] = 'custom_timezone_admin_submit';
  return system_settings_form($form);
}

/**
 * Function to handle admin form submit.
 */
function custom_timezone_admin_submit(&$form, &$form_state) {
  // Save settings for customised forms.
  variable_set('custom_timezone_alter_userform', $form_state['values']['custom_timezone_customised_forms']['alter_userform']);
  variable_set('custom_timezone_alter_datefield', $form_state['values']['custom_timezone_customised_forms']['alter_datefield']);

  // Get the current list so we can maintain same order of list.
  $timezonelist = variable_get('custom_timezone_timezonelist');
  $timezonelistupdated = array();

  // Create updated array and save custom timezone list.
  foreach ($timezonelist as $key => $value) {
    $timezonelistupdated[$key] = (array(
    'status' => $form_state['values']['custom_timezone_timezonelists'][$key],
    'label' => $form_state['values']['custom_timezone_label'][$key],
      )
    );
  }
  variable_set('custom_timezone_timezonelist', $timezonelistupdated);
}
